package com.cqc.swiperefreshlayoutdemo;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private List<String> list = new ArrayList<>();
    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
        initData();

        initRecyclerView();

        initSwipeRefreshLayout();
    }

    private void findViews() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new MyAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void initData() {
        list.clear();
        for (int i = 0; i < 20; i++) {
            list.add("item" + i);
        }
    }

    private void initSwipeRefreshLayout() {
        //设置下拉圆圈：是否缩放，出现的位置，结束的位置(进度条在Y轴的展示范围)
        swipeRefreshLayout.setProgressViewOffset(true, 10, 30);
        //设置圆圈的背景色
        swipeRefreshLayout.setProgressBackgroundColorSchemeColor(getResources().getColor(android.R.color.white));
//        swipeRefreshLayout.setProgressBackgroundColor(android.R.color.white);
//        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(android.R.color.white);

        //设置下拉圆圈的大小，只有2种常量LARGE + DEFAULT，设置具体的数字无效
        swipeRefreshLayout.setSize(SwipeRefreshLayout.LARGE);

        //设置下拉圆圈的颜色变化,默认白底黑圈进度条，不是setColorSchemeColors()
        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //刷新过程2秒
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        list.add(0,"item_add");
                        adapter.notifyItemInserted(0);
                        recyclerView.scrollToPosition(0);//回到顶部
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });
    }


    public class MyAdapter extends RecyclerView.Adapter<MyHolder> {

        @Override
        public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.item_main, parent, false);
            MyHolder holder = new MyHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(MyHolder holder, int position) {
            holder.tv.setText(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }


    public class MyHolder extends RecyclerView.ViewHolder {

        public TextView tv;

        public MyHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.tv);
        }
    }
}
