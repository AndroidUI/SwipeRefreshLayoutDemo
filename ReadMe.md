##**简介：SwipeRefreshLayout** ##
> SwipeRefreshLayout是google新出的下拉刷新的控件，但不可以上拉加载更多。

###**相关的方法：** ###
设置进度条的方法有:（进度条就是圆圈）
	
	setProgressBackgroundColor(int resId):进度条背景色
	setProgressBackgroundColorSchemeColor(int color):进度条背景色
	setProgressBackgroundColorSchemeResource(int colorRes):进度条背景色

	setProcessViewOffSet(boolean scale,int start,int end)：:进度条出现的时候是否缩放，Y轴的偏移量范围
	setSize(int size)圆圈的大小，2个默认值：LARGE + DEFAULT，设置具体的数字无效
	setColorSchemeResources(int... colorResIds)：圆圈的颜色，白底黑圈（最多4种颜色）
其他方法：

	setRefreshing(boolean refreshing)：控制刷新状态(True:进度条一直存在；false：进度条消失)
	isRefreshing()：正在刷新中（返Yes/NO）

## **接下来，通过案例演示** ##
> SwipeRefeshLayout内包裹RecyclerView,下拉刷新出现进度条，并添加数据。

### 效果图： ###
![这里写图片描述](http://img.blog.csdn.net/20160830165638893)
### 步骤： ###
1 找到这2个控件
2 设置recyclerview
3 设置swipeRefreshLayout（设置进度条+监听）

### 代码： ###
activity_main.xml
```
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="com.cqc.swiperefreshlayoutdemo.MainActivity">

    <android.support.v4.widget.SwipeRefreshLayout
        android:id="@+id/swipeRefreshLayout"
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <android.support.v7.widget.RecyclerView
            android:id="@+id/recyclerView"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_below="@id/swipeRefreshLayout">

        </android.support.v7.widget.RecyclerView>
    </android.support.v4.widget.SwipeRefreshLayout>
    
</RelativeLayout>

```

item_main.xml
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
              android:layout_width="match_parent"
              android:layout_height="50dp"
              android:orientation="vertical">

    <TextView
        android:id="@+id/tv"
        android:layout_width="match_parent"
        android:layout_height="45dp"/>

    <View
        android:layout_width="match_parent"
        android:layout_height="5dp"
        android:background="#FF4081"/>
</LinearLayout>

```
初始化recyclerView:
```
private void initRecyclerView() {
    recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    adapter = new MyAdapter();
    recyclerView.setAdapter(adapter);
}
```

初始化SwipeRefreshLayout
```
 private void initSwipeRefreshLayout() {
        //设置下拉圆圈：是否缩放，出现的位置，结束的位置(进度条在Y轴的展示范围)
        swipeRefreshLayout.setProgressViewOffset(true, 10, 30);
        //设置圆圈的背景色
        swipeRefreshLayout.setProgressBackgroundColorSchemeColor(getResources().getColor(android.R.color.white));
//        swipeRefreshLayout.setProgressBackgroundColor(android.R.color.white);
//        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(android.R.color.white);

        //设置下拉圆圈的大小，只有2种常量LARGE + DEFAULT，设置具体的数字无效
        swipeRefreshLayout.setSize(SwipeRefreshLayout.LARGE);

        //设置下拉圆圈的颜色变化,默认白底黑圈进度条，不是setColorSchemeColors()
        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //刷新过程2秒
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        list.add(0,"item_add");
                        adapter.notifyItemInserted(0);
                        recyclerView.scrollToPosition(0);//回到顶部
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });
    }
```
> 源码地址：https://github.com/s1168805219/SwipeRefreshLayoutDemo



